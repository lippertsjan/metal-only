# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The subsections are formatted as they need when publishing a release to the Play Store.

## [unreleased]

## [1.6.5] - 2023-12-01

* Fixed: UI errors in Streaming Fragment
    * Also extracted multiple methods and added a few test

## [1.6.4] - 2023-11-30

* Fixed: sending feedback

## [1.6.3.1] - 2023-11-29

* Fixed: Rewrote Stream with mediax
* Internal
    * Removed: unsafe TrustManager

## [1.6.3] - 2023-11-29

* Fixed: Rewrote Stream with mediax

## [1.6.2] - 2023-11-09

* Fixed: Backup & Mailer

## [1.6.1] - 2023-10-05

* Changed streaming urls to HTTPs
* Updated dependencies

## [1.6.0] - 2023-02-04

* Updated dependencies
* Lots of code maintenance
* Always load moderator images from the internet

## [1.5.2] - 2021-07-06

* Fixed: quality selection is now loaded correctly when opening preferences (#192)
* Added: version in about screen
* Fixed: now using adaptable icons as per developer guide (#191)

## [1.5.1] - 2021-01-10

* Added settings to menu
* Changed: Improved UI bug in stream quality selection
* Changed: general maintenance and cleanup
* Changed: added labels to bottom nav in stream fragment; removed the entries from the menu (#182)
* Added show information to wish fragment (#42)
* Fixed some crashes

## [1.5.0] - 2020-11-21

* Added stream quality can now be selected
* Fixed spacing in "on air"-text
* Changed: minor layout fixes

## [1.4.2] - 2020-09-08

* Fixed IllegalArgumentException crash
* Fixed wrong states by fixing access to exo player

## [1.4.1] - 2020-08-26

* Fixed: stream state in UI

## [1.4.0] - 2020-08-19

* Changed: New stream URLs
* Changed: Using ExoPlayer for Streaming now

## [1.3.3] - 2020-08-01

* Promotion of 1.3.2-5 to production
* Fixed: crash on some devices when streaming errors occurr
* Fixed: some problems caused by shrinking, minification (smaller app size)
* Fixed: multiple crash sources
* Changed: more null-safety
* Added: minification and resource shrinking
* Added: support for landscape mode

## [1.3.2-5] - 2020-07-30

* Fixed: some problems caused by shrinking, minification (smaller app size)

## [1.3.2-4] - 2020-07-21

* Fixed: crash on some devices when streaming errors occurr

## [1.3.2-3] - 2020-07-20

* Fixed: some problems caused by shrinking, minification (smaller app size)

## [1.3.2-2] - 2020-07-18

* Fixed: some crashes
* Changed: more null-safety

## [1.3.2-1] - 2020-07-18

* Re-Release of 1.3.1-1 with higher version

## [1.3.2] - 2020-07-18

* Improved: Actually fixed landscape layout

## [1.3.1-1] - 2020-07-18

* Added: minification and resource shrinking

## [1.3.1] - 2020-07-18

* Improved: landscape mode

## [1.3.0] - 2020-07-16

* Added: support for landscape mode
* Fixed: multiple crash sources

## [1.2.2] - 2020-01-17

* Fixed: Show and Genre are now displayed correctly

## [1.2.1] - 2019-12-31

* Fixed: smaller issues when wishing

## [1.2.0] - 2019-11-11

* Added: dark mode & theme preferences in "About the app"

## [1.1.4] - 2019-11-10

* Added: wishing from favorites
* Added: import of favorites

## [1.1.3] - 2019-11-04

* Fixed: NPEs and other crashes

## [1.1.2] - 2019-10-20

* Added: Favorites list to manage favorites.
* Added: Favorites can be added from stream control fragment
* Fixed: Pre-Release-Crashes

## [1.1.1] - 2019-10-19

* Added: Favorites list to manage favorites.
* Added: Favorites can be added from stream control fragment
* Fixed: Pre-Release-Crashes

## [1.1.0] - 2019-10-19

* Added: Favorites list to manage favorites.
* Added: Favorites can be added from stream control fragment

## [1.0.2] - 2019-10-06

* Fixed: onError was accidentally still using debug code and crashing app
* Fixed: some NPE exceptions in callbacks
* Fixed: DeadRemote exception in MoStreamingService
* Fixed: Client calls could wrap `null` as success

## [1.0.1] - 2019-09-30

* Fixed: mod image loading in plan
* Fixed: overlapping info in plan

<de-DE>
Komplettes Rewrite der App in neuem Design. Support für neue Android-Versionen (>8). Batterie-Verbrauch deutlich gesenkt.  Probleme bei der Darstellung des neuen Plans behoben.
</de-DE>

## [1.0.0] - 2019-09-30

* Added: plan
* Fixed: loading failed handling
* Fixed: play button was wrong right after start
* Added: basic plan, no offline handling
* Fixed: Wish fragment showed wrong things
* Added: Better navigation

<de-DE>
Komplettes Rewrite der App in neuem Design. Support für neue Android-Versionen (>8). Batterie-Verbrauch deutlich gesenkt. 
</de-DE>

<en-GB>
Complete rewrite of the app in a new design. Added support for new Android versions (>8). Battery usage was reduced drastically. English translation is in progress.
</en-GB>

<en-US>
Complete rewrite of the app in a new design. Added support for new Android versions (>8). Battery usage was reduced drastically. English translation is in progress.
</en-US>

## [0.9.33] - 2019-09-01

* Added: smaller screen support
* Internal: Improved navigation
* Added: basic wish functionality

## [0.9.32] - 2019-09-01

* Added: support for changing network
* UI styling
* Internal: Switched to navigation component

## [0.9.31] - 2019-08-30

* Fixed: Binding to service after resuming activity

## [0.9.30] - 2019-08-28

* Fixed: IllegalStateException when stopping
* Added: Info about loading show info

## [0.9.29] - ??

(Unknown)

## [0.9.28] - 2019-08-27

* Fixed: logs were to big for intents
* Fixed: Non-ending service
* Fixed: service binding
* Improved: audio focus handling
* Added: more debug statements to investigate doze
* Added: More debug statements to prepare "stream is buffering" UI
* Cleanup in Streaming service

## [0.9.27] - 2019-08-19 23:38

* Improved logging
* Cleanup in Streaming service, fixed non-ending service
* Fixed service binding
* More debug statements to investigate doze
* Improved audio focus handling
* More debug statements to prepare "stream is buffering" UI

## [0.9.26] - 2019-08-19

Alpha Release 1.

## [0.9.25] - 2019-08-17 18:21

* Using HyperLog

## [0.9.24] - 2019-08-17 18:11

* Fixed log locking...

## [0.9.23] - 2019-08-17 18:05

* Improved logging

## [0.9.22] - 2019-08-17 13:41

* Increased log file max size to 64k
* Added MoStreamingService.IsAwakeLogThread

## [0.9.21] - 2019-08-17 12:25

* Removed app whitelisting again. No difference.
* Reduced <main></main>x log file size to 8k
* Now using stopforeground in service
* Added stopwithtask to service

## [0.9.20] - 2019-08-17 01:48

* Another attempt on whitelisting request

## [0.9.19] - 2019-08-17 01:29

* Added: request for battery whitelisting

## [0.9.18] - 2019-08-17 00:37

* Fixed. Used wrong cast for AIDL binding

## [0.9.17] - 2019-08-17 00:21

* Moved service to own process; communication via AIDL

## [0.9.16] - 2019-08-16 14:59

* Added missing permission

## [0.9.15] - 2019-08-16 14:36

* Acquiring multicastLock too

## [0.9.14] - 2019-08-16 14:15

* Reduced log file max size to 256K
* Explicit wakelock

## [0.9.13] - 2019-08-17 13:

* Using WIFI_MODE_FULL_HIGH_PERF for wifi lock

## [0.9.12] - 2019-08-17 13:42

* Added more log statements

## [0.9.11] - 2019-08-16 13:21

* Added log statements and changed notification id

## [0.9.10] - 2019-08-16 13:06

* Actually starting service as foreground with immediate promotion

## [0.9.9] - 2019-08-16 12:32

* Starting service as foreground with immediate promotion

## [0.9.8] - 2019-08-16

* Removed notification sound, fixed priority
* Added version to log

## [0.9.7] - 2019-08-15

* Added show info updates
* Solved todos

## [0.9.6] - 2019-08-15

* Resolved warnings, fixed play state?

## [0.9.5] - 2019-08-15

* Added audio focus callbacks
* Added noisy stream bc receiver

## [0.9.4] - 2019-08-15

* Added wakelock and wifilock
* Added error display in ui

## [0.9.3] - 2019-08-12

<de-DE>
Internal Testing Release. Added notification.
</de-DE>

<en-GB>
Internal Testing Release. Added notification.
</en-GB>

<en-US>
Internal Testing Release. Added notification.
</en-US>
