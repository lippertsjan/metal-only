#!/bin/bash
FRACTION=${1:-0.5}
echo "Promoting beta artifact to $FRACTION of users."
./gradlew promoteArtifact --update production --user-fraction "$FRACTION"
