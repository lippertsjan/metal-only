package de.ironjan.metalonly.streaming;

import de.ironjan.metalonly.streaming.IStreamChangeCallback;
import de.ironjan.metalonly.streaming.ExoPlayerState;

/** Base interface for the streaming service. */
interface IStreamingService {
    /*
     * Starts playing the stream and updates the callback to {@code cb}.
     * @paran cb the new callback implementation
     */
    void play(in IStreamChangeCallback cb, in int qualityMode);
    /* Stops playing the stream. */
    void stop();
    /*
     * Stops playing the stream and updates the callback to {@code cb}.
     * @paran cb the new callback implementation
     */
    void stopWithCallback(in IStreamChangeCallback cb);

    /* @return {@code true} if the stream is playing or preparing. {@code false} otherwise. */
    boolean getIsPlayingOrPreparing();
    /* @return the last error message or {@code null} when there was no error. */
    String getLastError();

    /* @return the stream's current  state. */
    ExoPlayerState getState();

    /* @return if the stream's current quality is high. */
    boolean isHighQualityStream();

    /*
     * Updates the callback to {@code cb}.
     * @paran cb the new callback implementation
     */
    void addCallback(in IStreamChangeCallback cb);
}
