package de.ironjan.metalonly.streaming;

import de.ironjan.metalonly.streaming.ExoPlayerState;

/**
  * Callback interface for whenever the stream's state changes.
  */
interface IStreamChangeCallback {
    /**
      * Notifies the callback about the new {@code state}.
      * @param state the stream's new state
      */
    void update(in ExoPlayerState state);
}
