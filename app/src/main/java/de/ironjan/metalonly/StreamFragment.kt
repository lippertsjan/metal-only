package de.ironjan.metalonly

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.ironjan.metalonly.api.model.ShowInfo
import de.ironjan.metalonly.api.model.Stats
import de.ironjan.metalonly.api.model.TrackInfo
import de.ironjan.metalonly.databinding.FragmentStreamBinding
import de.ironjan.metalonly.databinding.FragmentStreamBottomNavigationBinding
import de.ironjan.metalonly.favorites.TrackFileHandler
import de.ironjan.metalonly.log.LW
import de.ironjan.metalonly.streaming.ExoPlayerInternalState
import de.ironjan.metalonly.streaming.ExoPlayerState
import de.ironjan.metalonly.streaming.IStreamChangeCallback
import de.ironjan.metalonly.streaming.IStreamingService
import de.ironjan.metalonly.streaming.MoStreamingService
import de.ironjan.metalonly.streaming.StateChangeCallback

// TODO: extract to strings.xml and adapt method
private const val DE_UNKNOW_ERROR = "Unbekannter Fehler"

class StreamFragment : Fragment(), StateChangeCallback, MainActivityTrackUpdater.TrackUpdate,
    MainActivityShowInfoUpdater.OnShowInfoUpdateCallback,
    StatsLoadingRunnable.StatsLoadingCallback {

    private var currentPlayButtonDrawableId: Int = R.drawable.ic_action_play;

    private var navigationBinding: FragmentStreamBottomNavigationBinding? = null
    private var binding: FragmentStreamBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStreamBinding.inflate(inflater, container, false)
        navigationBinding = FragmentStreamBottomNavigationBinding.bind(binding!!.root)
        return binding?.root
    }

    // region lifecycle methods

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val lContext = context ?: return
        mainActivityShowInfoUpdater = MainActivityShowInfoUpdater(lContext, this)
        mainActivityTrackUpdater = MainActivityTrackUpdater(lContext, this)
    }

    override fun onResume() {
        super.onResume()
        if (view != null) {
            refreshUi()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refreshUi()
        binding?.imageView?.setOnClickListener { onModClicked() }

        navigationBinding?.imgActionPlan?.setOnClickListener {
            (activity as MainActivity).onNavigationViaId(
                R.id.navigate_to_plan
            )
        }
        navigationBinding?.imgActionDonation?.setOnClickListener {
            (activity as MainActivity).onNavigationViaId(
                R.id.navigate_to_donation
            )
        }
        navigationBinding?.imgActionWish?.setOnClickListener {
            (activity as MainActivity).onNavigationViaId(
                R.id.navigate_to_wish
            )
        }
    }

    private fun onModClicked() {
        refreshUi()
    }

    private fun refreshUi() {
        runOnUiThread {
            binding?.txtShow?.setText(R.string.loadingShowInfo)
            binding?.txtGenre?.text = ""
            binding?.txtTrack?.text = ""
        }
        val lContext = context ?: return
        Thread(StatsLoadingRunnable(lContext, this)).start()

        lifecycle.addObserver(mainActivityShowInfoUpdater)
        lifecycle.addObserver(mainActivityTrackUpdater)

        updateTxtError()

        if (mBound) {
            val state = moStreamingService.state
            update(state)
        } else {
            Intent(lContext, MoStreamingService::class.java).also {
                it.action = MoStreamingService.ACTION_PLAY
                it.putExtra(MetalOnlyApplication.PREF_KEY_QUALITY, getQualityModeFromPrefs())

                lContext.bindService(it, connection, 0)
                LW.d(TAG, "onResume - binding to service if it exists.")
            }
        }
        binding?.fab?.setOnClickListener { togglePlaying() }
    }

    override fun onStop() {
        super.onStop()
        if (mBound) {
            mBound = false
            val lContext = context ?: return
            lContext.unbindService(connection) // FIXME service leak?
        }
    }

    override fun IsResumed(): Boolean = isResumed

    private lateinit var mainActivityShowInfoUpdater: MainActivityShowInfoUpdater
    private lateinit var mainActivityTrackUpdater: MainActivityTrackUpdater
    // endregion

    // region stats callbacks and mod image loading
    override fun onStatsLoadingError() {
        runOnUiThread {
            binding?.txtShow?.setText(R.string.message_could_not_load_mod_image)
        }
    }

    override fun onStatsLoadingSuccess(stats: Stats) {
        LW.d(TAG, "Showing stats")
        val track = stats.track
        val trackAsString = "${track.artist} - ${track.title}"
        val showInformation = stats.showInformation

        runOnUiThread {
            binding?.txtShow?.text = showInformation.show
            binding?.txtGenre?.text = showInformation.genre
            binding?.txtTrack?.text = trackAsString
        }
        if (isAdded) {
            setModerator(showInformation.moderator)
            LW.d(TAG, "Loading stats succeeded. Triggering mod image load.")

            loadModeratorImageInto(stats.showInformation.moderator, binding?.imageView)
        }

        updateFavoriteStar(track)
    }

    private fun updateFavoriteStar(track: TrackInfo) {
        val lContext = context ?: return
        val trackFileHandler = TrackFileHandler(lContext)
        val isFavorite = trackFileHandler.load().contains(track)
        if (isFavorite) {
            runOnUiThread {
                binding?.imgFavorited?.setImageResource(R.drawable.ic_is_favorite)

                binding?.imgFavorited?.setOnClickListener {
                    trackFileHandler.remove(track)
                    updateFavoriteStar(track)
                }
            }
        } else {
            runOnUiThread {
                binding?.imgFavorited?.setImageResource(R.drawable.ic_non_favorite)

                binding?.imgFavorited?.setOnClickListener {
                    trackFileHandler.add(track)
                    updateFavoriteStar(track)
                }
            }
        }
    }

    // endregion StatsLoadingCallback

    // region track update callbacks
    override fun onTrackChange(trackInfo: TrackInfo) {
        val s = "${trackInfo.artist} - ${trackInfo.title}"
        runOnUiThread {
            binding?.txtTrack?.text = s
        }
        updateFavoriteStar(trackInfo)
        LW.d(TAG, "Track info updated: $s")
    }
    // endregion

    // region show info update callbacks
    override fun onShowInfoChange(showInfo: ShowInfo) {
        if (isAdded) {
            setModerator(showInfo.moderator)
        }
        runOnUiThread {
            binding?.txtShow?.text = showInfo.show
            binding?.txtGenre?.text = showInfo.genre
        }
    }
    // endregion

    // region state handling and state change callback
    override fun update(state: ExoPlayerState) {
        LW.d(TAG, "update($state) called.")

        val newPlayButtonDrawable = getNewPlayButtonDrawable(state, currentPlayButtonDrawableId);
        currentPlayButtonDrawableId = newPlayButtonDrawable;
        val isBufferCircleRunning = getIsBufferCircleRunning(state);
        val qualityString = getQualityText(state);
        val qualityHint = getQualityHint(state)
        val txtStreamQualityVisibility =
            if (BuildConfig.DEBUG && state.isPlaying) View.VISIBLE else View.GONE


        runOnUiThread {
            binding?.let { binding ->
                binding.fab.setImageResource(newPlayButtonDrawable)

                binding.bufferingState.apply {
                    visibility = if (isBufferCircleRunning) View.VISIBLE else View.GONE
                    invalidate()
                }

                binding.txtStreamQuality.let { txt ->
                    txt.setText(qualityString)
                    txt.setHint(qualityHint)
                    txt.visibility =
                        txtStreamQualityVisibility
                }
            }
            if (state.state == ExoPlayerInternalState.Error) {
                showError()
            }

        }
    }


    private fun showError() {
        val lastError = if (mBound) {
            moStreamingService.lastError
        } else {
            DE_UNKNOW_ERROR
        }
        runOnUiThread {
            binding?.txtError?.text = lastError
        }
    }

    private var localState: ExoPlayerState =
        ExoPlayerState(ExoPlayerInternalState.Unknown, false, false, false)
    private val localStateIsPlayingOrPreparing
        get() = localState.isLoading || localState.isPlaying

    // endregion

    // region stream control
    private fun togglePlaying() {
        try {
            if (mBound) {
                togglePlayingViaServiceBind()
            } else {
                if (localStateIsPlayingOrPreparing) {
                    stopPlayBackViaIntent()
                } else {
                    LW.d(
                        TAG,
                        "Service is not bound and local state represents can Play. Starting and binding."
                    )
                    startAndBindStreamingService()
                }
            }
        } catch (e: Exception) {
            LW.e(TAG, "Toggle play failed.", e)
        }
    }

    private fun stopPlayBackViaIntent(): Boolean {
        LW.d(
            TAG,
            "Service is not bound but local state is playing or preparing. Stopping via intent."
        )
        val lContext = context ?: return true
        Intent(lContext, MoStreamingService::class.java).also {
            it.action = MoStreamingService.ACTION_STOP

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                lContext.startForegroundService(it)
                LW.d(TAG, "Running on Android O+, sending intent as foreground.")
            } else {
                lContext.startService(it)
                LW.d(
                    TAG,
                    "Running on Android before O, sending intent via startService."
                )
            }
            update(ExoPlayerState(ExoPlayerInternalState.Ended, false, false, false))

        }
        return false
    }

    private fun togglePlayingViaServiceBind() {
        if (moStreamingService.isPlayingOrPreparing) {
            LW.d(TAG, "Service is bound and playing or preparing, directly calling stop().")
            moStreamingService.stop()
            LW.d(TAG, "Stopped playing")
            return
        }

        LW.d(TAG, "Service is bound and can play, directly calling play(callback).")
        val qualityMode = getQualityModeFromPrefs()
        moStreamingService.play(asIStreamChangeCallback(), qualityMode)
        LW.d(TAG, "Started playing")
    }

    private fun getQualityModeFromPrefs(): Int {
        val prefs = activity?.getSharedPreferences(
            MetalOnlyApplication.SHARED_PREF_NAME, Context.MODE_PRIVATE
        )
        val qualityMode = prefs?.getInt(
            MetalOnlyApplication.PREF_KEY_QUALITY, MetalOnlyApplication.PREF_VAL_QUALITY_DEFAULT
        ) ?: MetalOnlyApplication.PREF_VAL_QUALITY_DEFAULT
        return qualityMode
    }

    private var mBound: Boolean = false

    private lateinit var moStreamingService: IStreamingService

    /** Defines callbacks for service binding, passed to bindService()  */
    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            moStreamingService = IStreamingService.Stub.asInterface(service)

            moStreamingService.addCallback(asIStreamChangeCallback())

            update(moStreamingService.state)

            updateTxtError()

            mBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }
    }

    private fun asIStreamChangeCallback(): IStreamChangeCallback.Stub {
        return object : IStreamChangeCallback.Stub() {
            override fun update(state: ExoPlayerState) {
                this@StreamFragment.update(state)
            }
        }
    }

    private fun startAndBindStreamingService() {
        val lContext = context ?: return
        Intent(lContext, MoStreamingService::class.java).also {
            it.action = MoStreamingService.ACTION_PLAY
            it.putExtra(MetalOnlyApplication.PREF_KEY_QUALITY, getQualityModeFromPrefs())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                lContext.startForegroundService(it)
                LW.d(TAG, "Running on Android O+, started service as foreground.")
            } else {
                lContext.startService(it)
                LW.d(TAG, "Running on Android before O, started service via startService.")
            }
            // FIXME leaking service connection
            lContext.bindService(it, connection, 0)
        }

        val state = ExoPlayerState(ExoPlayerInternalState.Buffering, true, false, false)
        update(state)
    }
    // endregion

    private fun updateTxtError() {
        runOnUiThread {
            if (mBound && moStreamingService.lastError != null) {
                binding?.txtError?.text = moStreamingService.lastError ?: ""
                binding?.txtError?.text = moStreamingService.lastError ?: ""
            } else {
                binding?.txtError?.text = ""
            }
        }
    }

    fun setModerator(moderator: String?) {
        (activity as MainActivity).setModerator(moderator)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun runOnUiThread(function: () -> Unit) = activity?.runOnUiThread(function)

    companion object {
        private const val TAG = "StreamFragment"

        internal fun getNewPlayButtonDrawable(state: ExoPlayerState, icActionPlay: Int): Int {
            val drawable = when (state.state) {
                ExoPlayerInternalState.Buffering -> R.drawable.ic_stream_loading
                ExoPlayerInternalState.Ready -> R.drawable.ic_action_stop
                ExoPlayerInternalState.Unknown -> icActionPlay
                else -> R.drawable.ic_action_play
            }
            return drawable
        }

        internal fun getIsBufferCircleRunning(state: ExoPlayerState): Boolean {
            return state.state == ExoPlayerInternalState.Buffering
        }

        internal fun getQualityText(state: ExoPlayerState): Int {
            return if (state.isHighQuality) {
                R.string.high_quality
            } else {
                R.string.low_quality
            }
        }

        internal fun getQualityHint(state: ExoPlayerState): Int {
            return if (state.isHighQuality) {
                R.string.desktop_stream
            } else {
                R.string.mobile_stream
            }
        }
    }
}
