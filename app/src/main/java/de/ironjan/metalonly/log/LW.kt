package de.ironjan.metalonly.log

import android.content.Context
import android.util.Log

object LW {
    var applicationContext: Context? = null

    fun `init`(context: Context) {
        val c = context.applicationContext
        applicationContext = c
    }

    fun v(tag: String, msg: String) {
        Log.v(tag, msg)
    }

    fun d(tag: String, msg: String) {
        Log.d(tag, msg)
    }

    fun w(tag: String, msg: String) {
        Log.w(tag, msg)
    }

    fun i(tag: String, msg: String) {
        Log.i(tag, msg)
    }

    fun e(tag: String, msg: String, e: Throwable? = null) {
        if (e != null) {
            Log.e(tag, msg, e)
        } else {
            Log.e(tag, msg)
        }
    }
}
