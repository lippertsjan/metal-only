package de.ironjan.metalonly

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import de.ironjan.metalonly.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    // region lifecycle methods

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_main)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)
    }
    // endregion

    // region options menu and navigation
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        if (onNavigationViaId(item.itemId)) true else super.onOptionsItemSelected(item)

    /**
     * Pass in a menu id.
     */
    fun onNavigationViaId(itemId: Int): Boolean {
        val navController = binding.navHostFragment.findNavController()
        when (itemId) {
            R.id.navigate_to_wish -> {
                if (navController.currentDestination?.id != R.id.wishFragment2) {
                    navController.navigate(R.id.to_wishFragment)
                }
                return true
            }

            R.id.navigate_to_plan -> {
                if (navController.currentDestination?.id != R.id.planFragment) {
                    navController.navigate(R.id.to_planFragment)
                }
                return true
            }

            R.id.mnuFavorites -> {
                if (navController.currentDestination?.id != R.id.favoritesFragment) {
                    navController.navigate(R.id.to_favoritesFragment)
                }
                return true
            }

            R.id.mnuAbout -> {
                if (navController.currentDestination?.id != R.id.planFragment) {
                    navController.navigate(R.id.to_aboutFragment)
                }
                return true
            }

            R.id.mnuSettings -> {
                if (navController.currentDestination?.id != R.id.planFragment) {
                    navController.navigate(R.id.to_aboutFragment)
                }
                return true
            }

            R.id.navigate_to_donation -> {
                openDonation()
                return true
            }

            R.id.mnuFeedback -> {
                sendFeedback(this)
                return true
            }

            else -> return false
        }
    }

    private fun openDonation() {
        val webpage: Uri = Uri.parse("https://www.metal-only.de/info-center/donation.html")
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    // endregion

    fun setModerator(moderator: String?) {
        runOnUiThread {
            if (moderator != null) {
                binding.actionBar.txtAbModerator.text = moderator
                binding.actionBar.txtAbLoading.visibility = View.GONE
                binding.actionBar.txtAbModerator.visibility = View.VISIBLE
                binding.actionBar.txtAbIs.visibility = View.VISIBLE
                binding.actionBar.txtAbOnAir.visibility = View.VISIBLE
            } else {
                binding.actionBar.txtAbLoading.visibility = View.VISIBLE
                binding.actionBar.txtAbModerator.visibility = View.GONE
                binding.actionBar.txtAbIs.visibility = View.GONE
                binding.actionBar.txtAbOnAir.visibility = View.GONE
            }
        }
    }
}
