package de.ironjan.metalonly

import android.widget.ImageView
import com.koushikdutta.ion.Ion

fun loadModeratorImageInto(moderatorName: String?, imageView: ImageView?) {
    if (moderatorName.isNullOrBlank()) {
        return
    }
    imageView ?: return

    imageView.post {
        val modUrl = "https://www.metal-only.de/botcon/mob.php?action=pic&nick=$moderatorName"
        Ion.with(imageView)
            .placeholder(R.drawable.metalhead)
            .load(modUrl)
    }
}
