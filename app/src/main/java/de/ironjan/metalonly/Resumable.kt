package de.ironjan.metalonly

interface Resumable {
    fun IsResumed(): Boolean
}
