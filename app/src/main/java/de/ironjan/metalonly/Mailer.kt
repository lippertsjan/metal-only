package de.ironjan.metalonly

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.widget.Toast

val developerMails = arrayOf("lippertsjan+mensaupb@gmail.com")

const val ANDROID_VERSION = "Android version"

const val ADDITIONAL_INFO = "Additional Info"

const val FEEDBACK = "Feedback"

const val SUBJECT_TEMPLATE = "Metal Only Feedback"

internal fun sendFeedback(context: Context?) {
    if (context == null) {
        return
    }
    val subject = String.format("$SUBJECT_TEMPLATE %s", BuildConfig.VERSION_NAME)

    val osVersion = Build.VERSION.RELEASE
    val manufacturer = Build.MANUFACTURER
    val model = Build.MODEL

    val info = "$manufacturer $model\n$ANDROID_VERSION: $osVersion"
    val body = "$FEEDBACK:\n\n\n---\n$ADDITIONAL_INFO:\n$info"

    sendMail(context, developerMails, subject, body)
}

const val MAILTO = "mailto:"

/**
 * Sends an email with the given subject and body. Notifies the user that an email app should be
 * set up, if there is none available.
 *
 * @param context a non-null context
 * @param to the receivers
 * @param subject The mail's subject
 * @param body The mail's body
 */
fun sendMail(
    context: Context,
    to: Array<String>,
    subject: String,
    body: String
) {
    val intent = Intent(Intent.ACTION_SENDTO, Uri.parse(MAILTO))
    intent.addCategory(Intent.CATEGORY_DEFAULT)
    intent.putExtra(Intent.EXTRA_EMAIL, to)
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.putExtra(Intent.EXTRA_TEXT, body)

    val hasMailAppInstalled = context.packageManager.queryIntentActivities(intent, 0).isNotEmpty()

    if (hasMailAppInstalled) {
        context.startActivity(intent)
    } else {
        Toast.makeText(context, "No mail app installed.", Toast.LENGTH_LONG).show()
    }
}
