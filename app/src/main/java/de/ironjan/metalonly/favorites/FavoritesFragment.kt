package de.ironjan.metalonly.favorites

import android.R.string.cancel
import android.R.string.ok
import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.ironjan.metalonly.R
import de.ironjan.metalonly.api.model.TrackInfo
import de.ironjan.metalonly.databinding.FragmentFavoritesBinding
import de.ironjan.metalonly.log.LW

class FavoritesFragment : Fragment() {
    private var binding: FragmentFavoritesBinding? = null
    private lateinit var adapter: FavoritesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val lContext = context ?: return
        adapter = FavoritesAdapter(lContext)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val lContext = context ?: return

        binding?.myRecyclerView?.apply {
            layoutManager = LinearLayoutManager(lContext)
            setHasFixedSize(true)
            adapter = this@FavoritesFragment.adapter
        }

        ItemTouchHelper(SwipeToDeleteCallback(adapter)).attachToRecyclerView(binding?.myRecyclerView)
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                super.onChanged()
                updateUi()
            }
        })

        refresh()
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    // region options menu and navigation
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorites_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        if (handleMenuItemClick(item.itemId)) true else super.onOptionsItemSelected(item)

    /**
     * Pass in a menu id.
     */
    fun handleMenuItemClick(itemId: Int): Boolean {
        if (R.id.mnuNewFav == itemId) {
            showNewFavoriteDialog()
            return true
        }
        return false
    }

    //endregion

    private fun showNewFavoriteDialog() {
        val lContext = context ?: return
        val builder = AlertDialog.Builder(lContext)
        builder.setTitle("Neuer Favorit")

        val viewInflated = LayoutInflater.from(lContext).inflate(R.layout.dialog_new_favorite, view as ViewGroup?, false)

        val editArtist = viewInflated.findViewById(R.id.editArtist) as EditText
        val editTitle = viewInflated.findViewById(R.id.editTitle) as EditText

        builder.setView(viewInflated)

        builder.setPositiveButton(ok) { dialog, which ->
            dialog.dismiss()
            val artist = editArtist.text.toString()
            val title = editTitle.text.toString()

            TrackFileHandler(lContext).add(TrackInfo(artist, title))

            refresh()
        }
        builder.setNegativeButton(cancel) { dialog, which -> dialog.cancel() }

        builder.show()
    }

    private fun refresh() {
        adapter.refresh()

        updateUi()
    }

    private fun updateUi() {
        activity?.runOnUiThread {
            if (adapter.itemCount == 0) {
                LW.d(TAG, "No favorites")
                binding?.empty?.visibility = View.VISIBLE
                binding?.myRecyclerView?.visibility = View.GONE
            } else {
                LW.d(TAG, "Showing ${adapter.itemCount} favorites.")
                binding?.empty?.visibility = View.GONE
                binding?.myRecyclerView?.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        const val TAG = "FavoritesFragment"
    }
}
