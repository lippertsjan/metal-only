package de.ironjan.metalonly.favorites

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import de.ironjan.metalonly.R
import de.ironjan.metalonly.api.model.TrackInfo

class FavoritesAdapter(val context: Context) : RecyclerView.Adapter<FavoritesViewHolder>() {
    private val trackFileHandler = TrackFileHandler(context)
    private val favs = mutableListOf<TrackInfo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_favorite_item, parent, false)

        return FavoritesViewHolder(view)
    }

    override fun getItemCount(): Int = favs.size

    override fun onBindViewHolder(holder: FavoritesViewHolder, position: Int) {
        val entry = favs[position]
        holder.itemView.findViewById<TextView>(R.id.txtArtistTitle).text = context.resources.getString(R.string.label_artist_title, entry.artist, entry.title)
        holder.itemView.setOnClickListener {
            click(entry, it)
        }
    }

    private fun click(entry: TrackInfo, it: View) {
        val dir = FavoritesFragmentDirections.toWishFragment(entry.artist, entry.title)
        it.findNavController().navigate(dir)
    }

    fun refresh() {
        val newFavs = trackFileHandler.load()
        favs.clear()
        favs.addAll(newFavs)
        notifyDataSetChanged()
    }

    fun delete(position: Int): TrackInfo {
        val deleted = favs[position]
        trackFileHandler.remove(deleted)
        refresh()
        return deleted
    }
}

class FavoritesViewHolder(view: View) : RecyclerView.ViewHolder(view)

/**
 * see https://medium.com/@zackcosborn/step-by-step-recyclerview-swipe-to-delete-and-undo-7bbae1fce27e
 */
class SwipeToDeleteCallback(private val adapter: FavoritesAdapter) :
    ItemTouchHelper.SimpleCallback(
        0,
        ItemTouchHelper.LEFT
            or ItemTouchHelper.RIGHT
            or ItemTouchHelper.DOWN
    ) {

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        TODO("not implemented") // To change body of created functions use File | Settings | File Templates.
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        /* FIXME add images for actions. trigger wish  in one direction, delete in the other. */
        when (direction) {
            ItemTouchHelper.LEFT -> {
                val deleted = adapter.delete(viewHolder.adapterPosition)
                val msg = "${deleted.artist} - ${deleted.title} wurde gelöscht."
                Toast.makeText(adapter.context, msg, Toast.LENGTH_LONG).show()
            }
            ItemTouchHelper.RIGHT -> {
                val deleted = adapter.delete(viewHolder.adapterPosition)
                val msg = "${deleted.artist} - ${deleted.title} wurde gelöscht."
                Toast.makeText(adapter.context, msg, Toast.LENGTH_LONG).show()
            }
            else -> { /* do nothing*/ }
        }
    }
}
