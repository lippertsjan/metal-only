package de.ironjan.metalonly.favorites

import android.content.Context
import arrow.core.Either
import arrow.core.getOrElse
import de.ironjan.metalonly.api.model.TrackInfo
import org.json.JSONObject
import java.io.FileNotFoundException
import java.io.InputStreamReader
import java.io.InvalidObjectException
import java.io.OutputStreamWriter
import java.util.LinkedList

class TrackFileHandler(private val context: Context) {

    fun load(): List<TrackInfo> {
        try {
            return parseRawData(loadRawContent())
        } catch (_: FileNotFoundException) {
            // nothing saved yet
            return listOf()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return listOf()
    }

    private fun parseRawData(tracksAsText: String): List<TrackInfo> =
        tracksAsText
            .split("\n\n")
            .filterNot { it.isBlank() }
            .mapNotNull {
                parseTrackInfo(it)
            }

    private fun parseTrackInfo(it: String): TrackInfo? {
        return try {
            val entry = it.split("\n")
            val artist = entry[0].replace("artist: ", "")
            val title = entry[1].replace("title: ", "")
            TrackInfo(artist, title)
        } catch (e: Exception) {
            throw InvalidObjectException("'$it' could not be parsed.")
        }
    }

    fun loadRawContent(): String {
        migrateFiles()

        val doesTrackFileExist = context.fileList().contains(V2_FILE_NAME)

        if (doesTrackFileExist) {
            val openFileInput = context.openFileInput(V2_FILE_NAME)
            val isr = InputStreamReader(openFileInput, Charsets.UTF_8)
            var tracksAsText = ""
            isr.buffered().use { r ->
                tracksAsText = r.readText()
            }
            return tracksAsText
        }

        return ""
    }

    private fun migrateFiles() {
        val hasV1Data = context.getFileStreamPath(V1_FILE_NAME)?.exists() ?: false
        val hasV2Data = context.getFileStreamPath(V2_FILE_NAME)?.exists() ?: false

        val v1ToV2 = !hasV2Data && hasV1Data

        if (v1ToV2) {
            val oldTracks = loadV1Favorites()

            save(oldTracks)
            //            todo delete old file
            //            context.deleteFile(SongSaver.JSON_FILE_FAV)
        }
    }

    private fun loadV1Favorites(): List<TrackInfo> {
        val v1Songs = readV1Songs()
        if (v1Songs.isLeft()) {
            // some error. ignore - is too old anyway
            return emptyList()
        }

        return v1Songs.getOrElse { emptyList() }
    }

    private fun save(tracks: List<TrackInfo>) {
        try {
            val tracksAsText =
                tracks
                    .sorted()
                    .map { "artist: ${it.artist}\ntitle: ${it.title}\n" }.joinToString("\n")

            // TODO is there an option to not use MODE_PRIVATE
            val osr = OutputStreamWriter(
                context.openFileOutput(V2_FILE_NAME, Context.MODE_PRIVATE),
                Charsets.UTF_8
            )
            osr.buffered().use { w -> w.write(tracksAsText) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun add(track: TrackInfo) {
        val entries = load()
        val listOf = arrayListOf(track)
        listOf.addAll(entries)
        save(listOf)
    }

    fun remove(track: TrackInfo) {
        val tracks = load().filterNot { it == track }
        save(tracks)
    }

    fun import(importText: String): ImportResult = try {
        save(parseRawData(importText))
        ImportResult(true)
    } catch (e: Exception) {
        ImportResult(false, "${e.message}")
    }

    data class ImportResult(val success: Boolean, val error: String? = null)

    companion object {
        const val V2_FILE_NAME = "favorites_v2.txt"
        const val V1_FILE_NAME = "mo_fav.json"
    }

    private fun readV1Songs(): Either<Throwable, List<TrackInfo>> {
        val JSON_ARRAY_SONGS = "songs"
        val JSON_STRING_INTERPRET = "interpret"
        val JSON_STRING_TITLE = "title"

        val isr: InputStreamReader
        try {
            isr = InputStreamReader(context.openFileInput(V1_FILE_NAME))
            var s = ""
            var read: Int
            val BUFF_SIZE = 256
            val buffer = CharArray(BUFF_SIZE)
            do {
                read = isr.read(buffer, 0, BUFF_SIZE)
                if (read > 0) s += String(buffer, 0, read)
            } while (read == BUFF_SIZE)
            isr.close()
            val rawFileContent = s
            val jObj = JSONObject(rawFileContent)

            val songs = jObj.getJSONArray(JSON_ARRAY_SONGS)

            val jsonObjects: LinkedList<JSONObject> = LinkedList()
            for (i in 0 until songs.length()) {
                jsonObjects.add(songs.getJSONObject(i))
            }

            val tracks = jsonObjects
                .mapNotNull {
                    val artist = it.getString(JSON_STRING_INTERPRET)
                    if (artist.isNullOrEmpty()) return@mapNotNull null

                    val title = it.getString(JSON_STRING_TITLE)
                    if (title.isNullOrEmpty()) return@mapNotNull null

                    TrackInfo(artist, title)
                }.sorted()

            return Either.Right(tracks)
        } catch (e: FileNotFoundException) {
            // everything is fine - just nothing saved
            return Either.Right(emptyList())
        } catch (e: Exception) {
            return Either.Left(e)
        }
    }
}
