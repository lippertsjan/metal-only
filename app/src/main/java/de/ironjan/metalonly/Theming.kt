package de.ironjan.metalonly

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Typography
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

object Theming {
 val ColorWarning = Color(0xFFCC0000)

    val MetalOnlyGreen = Color(0xFF00B215)

    val MetalOnlyGreenDarker = Color(0xFF008205)

    val MetalOnlyGreenDarkest = Color(0xFF005501)


    val DarkBackground = Color(0xFF121212)
    val DarkSurface = Color(0xFF1E1E1E)
    val DarkOnBackground = Color.White
    val DarkOnSurface = Color.White
    val DarkPrimary = Color(0xffe0e0e0)
    val DarkOnPrimary = Color.Black
    val DarkSecondary = MetalOnlyGreenDarker
    val DarkOnSecondary = Color.Black
    val DarkTertiary = MetalOnlyGreen
    val DarkOnTertiary = Color.Black

    val LightBackground = Color.White
    val LightSurface = Color.White
    val LightOnBackground = Color.Black
    val LightOnSurface = Color.Black
    val LightPrimary = Color(0xFF212121)
    val LightOnPrimary = Color.White
    val LightSecondary = MetalOnlyGreenDarkest
    val LightOnSecondary = Color.White
    val LightTertiary = MetalOnlyGreen
    val LightOnTertiary = Color.White


    val DarkColorScheme = darkColorScheme(
        primary = DarkPrimary,
        onPrimary = DarkOnPrimary,
        secondary = DarkSecondary,
        onSecondary = DarkOnSecondary,
        tertiary = DarkTertiary,
        onTertiary = DarkOnTertiary,
        background = DarkBackground,
        onBackground = DarkOnBackground,
        surface = DarkSurface,
        onSurface = DarkOnSurface
    )

    val LightColorScheme = lightColorScheme(
        primary = LightPrimary,
        onPrimary = LightOnPrimary,
        secondary = LightSecondary,
        onSecondary = LightOnSecondary,
        tertiary = LightTertiary,
        onTertiary = LightOnTertiary,
        background = LightBackground,
        onBackground = LightOnBackground,
        surface = LightSurface,
        onSurface = LightOnSurface
    )

    val MyTypography = Typography(
        bodyLarge = TextStyle(
            fontFamily = FontFamily.Default,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp,
            lineHeight = 24.sp,
            letterSpacing = 0.5.sp
        ),
        titleLarge = TextStyle(
            fontFamily = FontFamily.Serif,
            fontWeight = FontWeight.Bold,
            fontSize = 22.sp,
            lineHeight = 28.sp,
            letterSpacing = 0.sp
        ),
        labelSmall = TextStyle(
            fontFamily = FontFamily.Monospace,
            fontWeight = FontWeight.Medium,
            fontSize = 11.sp,
            lineHeight = 16.sp,
            letterSpacing = 0.5.sp
        )
    )


    @Composable
    public fun MyAppTheme(
        darkTheme: Boolean = isSystemInDarkTheme(),
        content: @Composable () -> Unit
    ) {
        val colorScheme = when {
            darkTheme -> DarkColorScheme
            else -> LightColorScheme
        }

        MaterialTheme(
            colorScheme = colorScheme,
            typography = MyTypography,
            content = content
        )
    }
}