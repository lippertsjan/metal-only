package de.ironjan.metalonly.api.model

import androidx.annotation.Keep

/**
 * Helper class to prevent invalid {@see PlanEntry}s.
 */
@Keep
data class RawPlanEntry(
    val start: String?,
    val end: String?,
    val showInformation: ShowInfo
) {
    private val isValid
        get() = !(start.isNullOrEmpty() || end.isNullOrEmpty())

    /**
     * Returns a valid {@see PlanEntry} or <c>null</c>.
     */
    val asPlanEntry
        get() = if (isValid) PlanEntry(start!!, end!!, showInformation) else null
}
