package de.ironjan.metalonly.api.model

data class Wish(
    val nick: String,
    val artist: String,
    val title: String,
    val greeting: String
)
