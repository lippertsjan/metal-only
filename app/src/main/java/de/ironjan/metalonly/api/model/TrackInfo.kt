package de.ironjan.metalonly.api.model

data class TrackInfo(
    val artist: String,
    val title: String
) : Comparable<TrackInfo> {
    override fun compareTo(other: TrackInfo): Int = toString().compareTo(other.toString())

    override fun toString(): String = "$artist - $title"
}
