package de.ironjan.metalonly.api

import android.content.Context
import arrow.core.Either
import com.google.gson.Gson
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.builder.Builders
import de.ironjan.metalonly.api.model.PlanEntry
import de.ironjan.metalonly.api.model.RawPlanEntry
import de.ironjan.metalonly.api.model.ShowInfo
import de.ironjan.metalonly.api.model.Stats
import de.ironjan.metalonly.api.model.TrackInfo
import de.ironjan.metalonly.api.model.Wish
import de.ironjan.metalonly.log.LW
import java.io.PrintWriter
import java.io.StringWriter
import java.io.Writer
import java.util.concurrent.TimeUnit

class Client(private val context: Context) {

    fun sendWish(wish: Wish, futureCallback: FutureCallback<String>) {
        Ion.with(context)
            .load(wishUrl)
            .setJsonPojoBody(wish)
            .asString(Charsets.UTF_8)
            .setCallback(futureCallback)
    }

    fun getPlan(): Either<String, List<PlanEntry>> {
        return try {
            val gson = Gson()
            val right = prepareRequest(planUrl)
                .asJsonArray()
                .get(REQUEST_TIMEOUT_30_SECONDS, TimeUnit.SECONDS)
                .map { gson.fromJson(it, RawPlanEntry::class.java) }
                .map { it.asPlanEntry }
                .filterNotNull()

            Either.Right(right)
        } catch (e: Exception) {
            wrapException(e)
        }
    }

    fun getStats(): Either<String, Stats> = safeRequest(statsUrl, Stats::class.java)

    fun getTrack(): Either<String, TrackInfo> = safeRequest(trackUrl, TrackInfo::class.java)

    fun getShowInfo(): Either<String, ShowInfo> = safeRequest(showInfoUrl, ShowInfo::class.java)

    private fun <T> safeRequest(url: String, clazz: Class<T>): Either<String, T> {
        return try {
            val right = prepareRequest(url)
                .`as`(clazz)
                .get(REQUEST_TIMEOUT_30_SECONDS, TimeUnit.SECONDS)

            if (right != null) {
                Either.Right(right)
            } else {
                Either.Left("Loading failed.")
            }
        } catch (e: Exception) {
            wrapException(e)
        }
    }

    private fun wrapException(e: Exception): Either<String, Nothing> {
        val sw: Writer = StringWriter()
        e.printStackTrace(PrintWriter(sw))
        LW.e("Client", "Request failed: ", e)
        return Either.Left(sw.toString())
    }

    private fun prepareRequest(url: String): Builders.Any.B {
        val requestBuilder = Ion.with(context).load(url)
        return requestBuilder.noCache()
    }

    companion object {
        const val REQUEST_TIMEOUT_30_SECONDS = 30000L
        private const val baseUrl = "https://mensaupb.herokuapp.com/metalonly"

        private const val statsPath = "/stats"

        private const val trackPath = "/track"
        private const val showInformationPath = "/showinformation"
        private const val planPath = "/plan"
        private const val wishPath = "/wish"

        const val statsUrl = "$baseUrl$statsPath"
        const val showInfoUrl = "$baseUrl$showInformationPath"
        const val trackUrl = "$baseUrl$trackPath"
        const val wishUrl = "$baseUrl$wishPath"
        const val planUrl = "$baseUrl$planPath"
    }
}
