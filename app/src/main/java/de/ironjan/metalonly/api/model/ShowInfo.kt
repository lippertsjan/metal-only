package de.ironjan.metalonly.api.model

import androidx.annotation.Keep

@Keep
data class ShowInfo(
    val moderator: String?,
    val show: String?,
    val genre: String?
)
