package de.ironjan.metalonly

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import de.ironjan.metalonly.log.LW

class MetalOnlyApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        LW.init(this)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        val prefs = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE) ?: return
        val theme_mode = prefs.getInt(PREF_KEY_THEME_MODE, PREF_VAL_THEME_MODE_DEFAULT)

        when (theme_mode) {
            PREF_VAL_THEME_MODE_AUTO -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY)
            PREF_VAL_THEME_MODE_DAY -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            PREF_VAL_THEME_MODE_NIGHT -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
    }

    companion object {
        const val SHARED_PREF_NAME = "metal_only_preferences"

        const val PREF_KEY_THEME_MODE = "PREF_KEY_THEME_MODE"
        const val PREF_VAL_THEME_MODE_AUTO = 0
        const val PREF_VAL_THEME_MODE_DAY = 1
        const val PREF_VAL_THEME_MODE_NIGHT = 2
        const val PREF_VAL_THEME_MODE_DEFAULT = PREF_VAL_THEME_MODE_AUTO

        const val PREF_KEY_QUALITY = "PREF_KEY_THEME_QUALITY"
        const val PREF_VAL_QUALITY_MOBILE = 0
        const val PREF_VAL_QUALITY_DESKTOP = 1
        const val PREF_VAL_QUALITY_AUTO = 2
        const val PREF_VAL_QUALITY_DEFAULT = PREF_VAL_QUALITY_MOBILE
    }
}
