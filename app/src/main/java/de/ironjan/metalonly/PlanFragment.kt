package de.ironjan.metalonly

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import de.ironjan.metalonly.api.Client
import de.ironjan.metalonly.api.model.PlanEntry
import de.ironjan.metalonly.databinding.FragmentPlanBinding
import de.ironjan.metalonly.log.LW
import de.ironjan.metalonly.plan.PlanRecyclerViewAdapter
import java.util.*

private const val MOD_FREE = "frei"

/**
 * FIXME add list
 * FIXME add empty view, loading fail, etc.
 */
class PlanFragment : Fragment() {

    private var binding: FragmentPlanBinding? = null
    private val adapter = PlanRecyclerViewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlanBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val lContext = context ?: return

        binding?.myRecyclerView?.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(lContext)
            adapter = this@PlanFragment.adapter
        }

        binding?.empty?.setOnClickListener { refresh() }
        refresh()
    }

    override fun onResume() {
        super.onResume()
        refresh()
    }

    private fun refresh() {
        val lContext = context ?: return

        AsyncTask.execute {
            val either = Client(lContext).getPlan()
            LW.d(TAG, "Start loading")

            if (either.isLeft()) {
                either.mapLeft {
                    LW.d(TAG, "Loading plan failed: $it")
                    loadingFailed()
                }
            } else {
                LW.d(TAG, "Loading done")
                either.map { showPlan(it) }
            }
        }
    }

    private fun showPlan(nullablePlan: List<PlanEntry>?) {
        val plan = nullablePlan ?: listOf()

        val now = Date()

        val filteredPlan =
            plan.filter { it.endDateTime?.after(now) ?: false }
                .filter { !MOD_FREE.equals(it.showInformation.moderator) }
                .sortedBy { it.start }

        activity?.runOnUiThread {
            adapter.setPlan(filteredPlan)
            binding?.empty?.visibility = View.GONE
            binding?.myRecyclerView?.visibility = View.VISIBLE
        }
    }

    private fun loadingFailed() {
        activity?.runOnUiThread {
            binding?.empty?.visibility = View.VISIBLE
            binding?.myRecyclerView?.visibility = View.GONE
        }
    }

    companion object {
        private const val TAG = "PlanFragment"
    }
}
