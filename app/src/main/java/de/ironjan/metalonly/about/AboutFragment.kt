package de.ironjan.metalonly.about

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import de.ironjan.metalonly.BuildConfig
import de.ironjan.metalonly.MetalOnlyApplication
import de.ironjan.metalonly.MetalOnlyApplication.Companion.PREF_KEY_QUALITY
import de.ironjan.metalonly.MetalOnlyApplication.Companion.PREF_KEY_THEME_MODE
import de.ironjan.metalonly.MetalOnlyApplication.Companion.PREF_VAL_QUALITY_DEFAULT
import de.ironjan.metalonly.MetalOnlyApplication.Companion.PREF_VAL_THEME_MODE_AUTO
import de.ironjan.metalonly.MetalOnlyApplication.Companion.PREF_VAL_THEME_MODE_DAY
import de.ironjan.metalonly.MetalOnlyApplication.Companion.PREF_VAL_THEME_MODE_DEFAULT
import de.ironjan.metalonly.MetalOnlyApplication.Companion.PREF_VAL_THEME_MODE_NIGHT
import de.ironjan.metalonly.R
import de.ironjan.metalonly.databinding.FragmentAboutBinding
import de.ironjan.metalonly.favorites.TrackFileHandler
import de.ironjan.metalonly.log.LW

class AboutFragment : Fragment() {

    private var binding: FragmentAboutBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAboutBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.txtVersion?.text =
            context?.getString(R.string.about_app_version_template, BuildConfig.VERSION_NAME)

        binding?.btnExportFavorites?.setOnClickListener { exportFavorites() }

        binding?.btnImportFavorites?.setOnClickListener { importFavorites() }

        binding?.themeModeSpinner?.apply {
            val prefs = activity?.getSharedPreferences(
                MetalOnlyApplication.SHARED_PREF_NAME,
                Context.MODE_PRIVATE
            ) ?: return

            setSelection(prefs.getInt(PREF_KEY_THEME_MODE, PREF_VAL_THEME_MODE_DEFAULT), false)
        }
        binding?.themeModeSpinner?.apply {
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    val prefs = activity?.getSharedPreferences(
                        MetalOnlyApplication.SHARED_PREF_NAME,
                        Context.MODE_PRIVATE
                    ) ?: return

                    if (position == prefs.getInt(
                            PREF_KEY_THEME_MODE,
                            PREF_VAL_THEME_MODE_DEFAULT
                        )
                    ) {
                        return
                    }

                    when (position) {
                        PREF_VAL_THEME_MODE_AUTO -> AppCompatDelegate.setDefaultNightMode(
                            AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
                        )
                        PREF_VAL_THEME_MODE_DAY -> AppCompatDelegate.setDefaultNightMode(
                            AppCompatDelegate.MODE_NIGHT_NO
                        )
                        PREF_VAL_THEME_MODE_NIGHT -> AppCompatDelegate.setDefaultNightMode(
                            AppCompatDelegate.MODE_NIGHT_YES
                        )
                    }

                    with(prefs.edit()) {
                        putInt(PREF_KEY_THEME_MODE, position)
                        apply()
                    }
                }
            }
        }

        binding?.qualityModeSpinner?.apply {
            val prefs = activity?.getSharedPreferences(
                MetalOnlyApplication.SHARED_PREF_NAME,
                Context.MODE_PRIVATE
            ) ?: return

            val prefQuality = prefs.getInt(PREF_KEY_QUALITY, PREF_VAL_QUALITY_DEFAULT)

            LW.d(TAG, "Got prefQuality = $prefQuality")
            val appliedQuality =
                if (prefQuality < 0 || 3 <= prefQuality) PREF_VAL_QUALITY_DEFAULT else prefQuality

            LW.d(TAG, "Got appliedQuality = $appliedQuality")
            setSelection(appliedQuality, false)
        }
        binding?.qualityModeSpinner?.apply {
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    selectedQualityMode: Int,
                    id: Long
                ) {
                    LW.d(TAG, "Selected quality mode: $selectedQualityMode")

                    val prefs = activity?.getSharedPreferences(
                        MetalOnlyApplication.SHARED_PREF_NAME,
                        Context.MODE_PRIVATE
                    ) ?: return

                    val currentPrefValue = prefs.getInt(PREF_KEY_QUALITY, PREF_VAL_QUALITY_DEFAULT)
                    LW.d(TAG, "Current pref value: $currentPrefValue")
                    if (selectedQualityMode == currentPrefValue) {
                        LW.d(
                            TAG,
                            "Current pref value is equal to selected value. No safe necessary."
                        )
                        return
                    }

                    with(prefs.edit()) {
                        putInt(PREF_KEY_QUALITY, selectedQualityMode)
                        apply()
                        LW.i(
                            TAG,
                            "Updated quality mode to $selectedQualityMode from $currentPrefValue."
                        )
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
        }
    }

    private fun exportFavorites() {
        val lContext = context ?: return

        val body = TrackFileHandler(lContext).loadRawContent()

        val clipboard = lContext.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText("metal only backup", body)
        clipboard.setPrimaryClip(clip)
    }

    private fun importFavorites() {
        // TODO file import would be better...
        val lContext = context ?: return
        val builder = AlertDialog.Builder(lContext)
        builder.setTitle(R.string.backup_import)

        val viewInflated = LayoutInflater.from(lContext)
            .inflate(R.layout.dialog_favorites_import, view as ViewGroup?, false)

        val editText = viewInflated.findViewById(R.id.editText) as EditText

        builder.setView(viewInflated)

        builder.setPositiveButton(android.R.string.ok) { dialog, _ ->
            dialog.dismiss()
            val importText = editText.text.toString()

            val (success, error) = TrackFileHandler(lContext).import(importText)
            if (success) {
                Toast.makeText(
                    lContext,
                    getString(R.string.backup_import_completed),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    lContext,
                    getString(R.string.backup_import_failed, error),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        builder.setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }

        builder.show()
    }

    companion object {
        const val TAG = "AboutFragment"
    }
}
