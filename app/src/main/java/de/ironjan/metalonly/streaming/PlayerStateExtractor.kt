package de.ironjan.metalonly.streaming

import androidx.media3.common.Player

object PlayerStateExtractor {

    fun getStateFrom(player: Player, isHighQuality: Boolean): ExoPlayerState {
        val exoPlayerInternalState = when (player.playbackState) {
            Player.STATE_IDLE -> ExoPlayerInternalState.Idle
            Player.STATE_BUFFERING -> ExoPlayerInternalState.Buffering
            Player.STATE_READY -> ExoPlayerInternalState.Ready
            Player.STATE_ENDED -> ExoPlayerInternalState.Ended
            else -> ExoPlayerInternalState.Unknown
        }
        return ExoPlayerState(
            exoPlayerInternalState,
            player.isLoading,
            player.isPlaying,
            isHighQuality
        )
    }
}
