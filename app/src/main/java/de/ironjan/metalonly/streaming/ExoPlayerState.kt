package de.ironjan.metalonly.streaming

import android.os.Parcel
import android.os.Parcelable

// TODO migrate to https://developer.android.com/kotlin/parcelize
data class ExoPlayerState(
    val state: ExoPlayerInternalState,
    val isLoading: Boolean,
    val isPlaying: Boolean,
    val isHighQuality: Boolean
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(ExoPlayerInternalState::class.java.classLoader)!!,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(state, flags)
        parcel.writeByte(if (isLoading) 1 else 0)
        parcel.writeByte(if (isPlaying) 1 else 0)
        parcel.writeByte(if (isHighQuality) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "ExoPlayerState(state: $state, isLoading: $isLoading, isPlaying: $isPlaying, isHighQuality: $isHighQuality)"
    }

    companion object CREATOR : Parcelable.Creator<ExoPlayerState> {
        override fun createFromParcel(parcel: Parcel): ExoPlayerState {
            return ExoPlayerState(parcel)
        }

        override fun newArray(size: Int): Array<ExoPlayerState?> {
            return arrayOfNulls(size)
        }
    }
}
