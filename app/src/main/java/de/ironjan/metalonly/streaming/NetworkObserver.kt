package de.ironjan.metalonly.streaming

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.telephony.TelephonyManager
import androidx.core.net.ConnectivityManagerCompat
import de.ironjan.metalonly.log.LW

class NetworkObserver(private val moStreamingService: MoStreamingService) : BroadcastReceiver() {
    private val connectivityManager =
        moStreamingService.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private var lastNetworkType: Int? = null
    private val lastNetworkTypeLock = Any()

    override fun onReceive(context: Context, intent: Intent) {
        val networkInfo =
            ConnectivityManagerCompat.getNetworkInfoFromBroadcast(connectivityManager, intent)

        val newNetworkType = networkInfo?.type ?: -1
        val newNetworkSubType = networkInfo?.subtype ?: -1

        val isNewNetworkFast = isConnectionFast(newNetworkType, newNetworkSubType)

        LW.i(TAG, "Received network change event to: $networkInfo, $newNetworkType.")

        synchronized(lastNetworkTypeLock) {
            if (lastNetworkType != newNetworkType) {
                moStreamingService.restartPlayback(isNewNetworkFast)
                lastNetworkType = newNetworkType
                LW.d(TAG, "Updated last known network type.")
            }
        }
    }

    companion object {
        private const val TAG = "NetworkObserver"

        /**
         * Check if the connection is fast, based on <a href="https://stackoverflow.com/a/60294265/1666181">stackoverflow.com</a>.
         * @param type
         * @param subType
         * @return
         */
        fun isConnectionFast(type: Int, subType: Int): Boolean = when (type) {
            ConnectivityManager.TYPE_WIFI -> {
                true
            }
            ConnectivityManager.TYPE_MOBILE -> {
                when (subType) {
                    TelephonyManager.NETWORK_TYPE_1xRTT -> false // ~ 50-100 kbps
                    TelephonyManager.NETWORK_TYPE_CDMA -> false // ~ 14-64 kbps
                    TelephonyManager.NETWORK_TYPE_EDGE -> false // ~ 50-100 kbps
                    TelephonyManager.NETWORK_TYPE_EVDO_0 -> true // ~ 400-1000 kbps
                    TelephonyManager.NETWORK_TYPE_EVDO_A -> true // ~ 600-1400 kbps
                    TelephonyManager.NETWORK_TYPE_GPRS -> true // ~ 100 kbps
                    TelephonyManager.NETWORK_TYPE_HSDPA -> true // ~ 2-14 Mbps
                    TelephonyManager.NETWORK_TYPE_HSPA -> true // ~ 700-1700 kbps
                    TelephonyManager.NETWORK_TYPE_HSUPA -> true // ~ 1-23 Mbps
                    TelephonyManager.NETWORK_TYPE_UMTS -> true // ~ 400-7000 kbps
                    TelephonyManager.NETWORK_TYPE_EHRPD -> true // ~ 1-2 Mbps
                    TelephonyManager.NETWORK_TYPE_EVDO_B -> true // ~ 5 Mbps
                    TelephonyManager.NETWORK_TYPE_HSPAP -> true // ~ 10-20 Mbps
                    TelephonyManager.NETWORK_TYPE_IDEN -> false // ~25 kbps
                    TelephonyManager.NETWORK_TYPE_LTE -> true // ~ 10+ Mbps
                    TelephonyManager.NETWORK_TYPE_UNKNOWN -> false
                    else -> false
                }
            }
            else -> {
                false
            }
        }
    }
}
