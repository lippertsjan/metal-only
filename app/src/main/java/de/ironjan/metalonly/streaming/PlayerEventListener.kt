package de.ironjan.metalonly.streaming

import androidx.media3.common.PlaybackException
import androidx.media3.common.Player
import de.ironjan.metalonly.log.LW

class PlayerEventListener(private val service: MoStreamingService) : Player.Listener {

    override fun onEvents(player: Player, events: Player.Events) {
        super.onEvents(player, events)
        LW.d(MoStreamingService.TAG, "onEvents($player, $events)")
        service.updateState(player)
    }

    override fun onPlayerError(error: PlaybackException) {
        super.onPlayerError(error)
        LW.d(MoStreamingService.TAG, "onPlayerError($error)")
        service.onError(error)
    }
}
