package de.ironjan.metalonly.streaming

interface StateChangeCallback {
    fun update(state: ExoPlayerState)
}
