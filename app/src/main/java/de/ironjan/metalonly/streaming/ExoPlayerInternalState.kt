package de.ironjan.metalonly.streaming

import android.os.Parcel
import android.os.Parcelable

enum class ExoPlayerInternalState : Parcelable {
    Unknown,
    Buffering,
    Idle,
    Ready,
    Ended,
    Error;

    override fun writeToParcel(parcel: Parcel, flags: Int) = parcel.writeString(name)

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ExoPlayerInternalState> {
        override fun createFromParcel(parcel: Parcel): ExoPlayerInternalState =
            valueOf(parcel.readString()!!)

        override fun newArray(size: Int): Array<ExoPlayerInternalState?> = arrayOfNulls(size)
    }
}
