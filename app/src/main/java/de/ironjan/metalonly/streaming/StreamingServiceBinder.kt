package de.ironjan.metalonly.streaming

/** AIDL based binder */
class StreamingServiceBinder(private val service: MoStreamingService) : IStreamingService.Stub() {
    override fun play(cb: IStreamChangeCallback, qualityMode: Int) = service.play(wrap(cb), qualityMode)
    override fun stop() = service.stop()
    override fun stopWithCallback(cb: IStreamChangeCallback) = service.stopWithCallback(wrap(cb))

    override fun getIsPlayingOrPreparing(): Boolean = service.isPlayingOrPreparing
    override fun getLastError(): String? = service.lastError

    override fun getState(): ExoPlayerState = service.state
    override fun isHighQualityStream(): Boolean {
        return service.isHighQualityStream
    }

    override fun addCallback(cb: IStreamChangeCallback?) {
        service.addStateChangeCallback(wrap(cb))
    }

    private fun wrap(cb: IStreamChangeCallback?): StateChangeCallback {
        return object : StateChangeCallback {
            override fun update(state: ExoPlayerState) {
                cb?.update(state)
            }
        }
    }
}
