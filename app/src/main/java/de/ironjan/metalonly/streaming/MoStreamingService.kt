package de.ironjan.metalonly.streaming

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ServiceInfo
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.os.RemoteException
import androidx.annotation.OptIn
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.media3.common.MediaItem
import androidx.media3.common.PlaybackException
import androidx.media3.common.Player
import androidx.media3.common.util.UnstableApi
import androidx.media3.common.util.Util
import androidx.media3.datasource.DefaultDataSourceFactory
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.ProgressiveMediaSource
import de.ironjan.metalonly.BuildConfig
import de.ironjan.metalonly.MainActivity
import de.ironjan.metalonly.MetalOnlyApplication
import de.ironjan.metalonly.R
import de.ironjan.metalonly.log.LW
import java.text.SimpleDateFormat
import java.util.Calendar

/**
 * streaming service. wraps interaction with media player. Use [IStreamingService.Stub] to bind.
 */
class MoStreamingService : Service() {

    private val errorState = ExoPlayerState(
        ExoPlayerInternalState.Error,
        isLoading = false,
        isPlaying = false,
        isHighQuality = false
    )

    private var exoPlayer: ExoPlayer? = null
    private var lockHandler: LockHandler? = null

    private var qualityMode = MetalOnlyApplication.PREF_VAL_QUALITY_DEFAULT
    var isHighQualityStream: Boolean = false
        private set

    private val connectivityManager
        get() = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        LW.d(TAG, "Handling start command with action '${intent?.action}'.")

        val intentQuality = intent?.extras?.getInt(MetalOnlyApplication.PREF_KEY_QUALITY)
        qualityMode = intentQuality
            ?: MetalOnlyApplication.PREF_VAL_QUALITY_DEFAULT
        LW.w(TAG, "Received intent. intentQuality = $intentQuality, qualityMode = $qualityMode")

        when (intent?.action) {
            ACTION_PLAY -> play()
            ACTION_STOP -> stop()
            else -> LW.d(TAG, "Received unknown action")
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()
        LW.init(this)

        LW.i(TAG, "onCreate called")

        promoteToForeground()

        lockHandler = LockHandler.acquire(this)

        myNoisyAudioStreamReceiver = BecomingNoisyReceiver(this)
        networkObserver = NetworkObserver(this)
        LW.d(TAG, "created NetworkObserver")

        LW.d(TAG, "onCreate done")
    }

    override fun onDestroy() {
        super.onDestroy()
        LW.d(TAG, "Service is destroyed now.")
    }

    // region binding
    override fun onBind(p0: Intent?): IBinder = binder

    private val binder = StreamingServiceBinder(this)
    // endregion

    // region state handling
    var lastError: String? = null

    var state: ExoPlayerState = ExoPlayerState(
        ExoPlayerInternalState.Unknown,
        isLoading = false,
        isPlaying = false,
        isHighQuality = false
    )
        private set(value) {
            field = value

            try {
                stateChangeCallback?.update(value)
            } catch (e: RemoteException) {
                LW.d(TAG, "RemoteException thrown: $e")
            }
        }

    private var stateChangeCallback: StateChangeCallback? = null

    val isPlayingOrPreparing: Boolean
        get() = state.isPlaying || state.isLoading

    fun addStateChangeCallback(cb: StateChangeCallback) {
        stateChangeCallback = cb
    }
    // endregion

    // region foreground service notification

    private fun promoteToForeground() {
        val pendingIntent = Intent(this, MainActivity::class.java).let { notificationIntent ->
            val flags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PendingIntent.FLAG_IMMUTABLE
            } else {
                0
            }

            PendingIntent.getActivity(this, 0, notificationIntent, flags)
        }
        val notification2 = NotificationCompat.Builder(this, ChannelId)
            .setContentTitle("Metal Only")
            .setContentText("Playing stream")
            .setSmallIcon(R.drawable.ic_notification)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setSound(null)
            .setOnlyAlertOnce(true)
            .build()

        createNotificationChannel()

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.TIRAMISU) {
            startForeground(
                NotificationId,
                notification2,
                ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
            )
        } else {
            startForeground(NotificationId, notification2)
        }
        LW.i(TAG, "Promoted service to foreground")
    }

    private fun createNotificationChannel() {
        LW.d(TAG, "createNotificationChannel called")

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = (NotificationChannelName)
            val descriptionText = ("Metal Only Stream Notification") // todo move to resource
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(ChannelId, name, importance).apply {
                description = descriptionText
                setSound(null, null)
            }
            // Register the channel with the system
            NotificationManagerCompat
                .from(this)
                .createNotificationChannel(channel)
            LW.d(TAG, "notification channel created")
        } else {
            LW.d(TAG, "api < O. no notification channel necessary")
        }
    }

    // endregion

    // region play stream
    fun play(cb: StateChangeCallback, qualityMode: Int) {
        LW.w(TAG, "Started via binding. qualityMode ${this.qualityMode} changes to $qualityMode")
        this.qualityMode = qualityMode
        addStateChangeCallback(cb)
        play()
    }

    private fun play() {
        LW.d(TAG, "play() called")

        lastError = null

        LW.d(TAG, "Service is in foreground now")

        runOnMain {
            updateState()

            exoPlayer?.apply {
                release()
                LW.d(TAG, "Released previous exoplayer.")
            }
        }

        synchronized(networkObserverLock) {
            registerReceiver(networkObserver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
            networkObserverRegistered = true
            LW.d(TAG, "Registered networkObserver")
        }

        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        val type = activeNetworkInfo?.type ?: -1
        val subType = activeNetworkInfo?.subtype ?: -1

        runOnMain {
            exoPlayer = createExoPlayer(NetworkObserver.isConnectionFast(type, subType))
        }
    }

    @OptIn(UnstableApi::class)
    private fun createExoPlayer(isNetworkFast: Boolean): ExoPlayer {
        LW.d(TAG, "Creating a new exoplayer.")

        val streamUri = when (qualityMode) {
            MetalOnlyApplication.PREF_VAL_QUALITY_MOBILE -> mobileAacStreamUri
            MetalOnlyApplication.PREF_VAL_QUALITY_DESKTOP -> desktopMp3StreamUri
            MetalOnlyApplication.PREF_VAL_QUALITY_AUTO -> if (isNetworkFast) desktopMp3StreamUri else mobileAacStreamUri
            else -> mobileAacStreamUri
        }

        isHighQualityStream = (streamUri == desktopMp3StreamUri)

        LW.w(
            TAG,
            "Created new exoplayer. qualityMode = $qualityMode, isNetworkFast = $isNetworkFast, isHighQualityStream = $isHighQualityStream"
        )

        val dataSourceFactory =
            DefaultDataSourceFactory(this, Util.getUserAgent(this, BuildConfig.APPLICATION_ID))
        val mediaSource = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(
            MediaItem.fromUri(Uri.parse(streamUri))
        )

        return ExoPlayer.Builder(this).build().apply {
            playWhenReady = true
            prepare(mediaSource)
            addListener(playerEventListener)
            LW.d(TAG, "Preparing the new exoplayer.")
        }
    }

    private val playerEventListener = PlayerEventListener(this)

    private fun updateState() {
        exoPlayer?.let { updateState(it) }
    }

    internal fun updateState(player: Player) {
        runOnMain {
            val stateFrom = PlayerStateExtractor.getStateFrom(player, isHighQualityStream)
            LW.d(TAG, "Updating state to $stateFrom")
            state = stateFrom
        }
    }

    // endregion

    // region error handling

    internal fun onError(e: PlaybackException) {
        LW.e(TAG, "onError called. Delegating to string based method for now. Error: $e")
        onError(e.message ?: "Unknown error.")
    }

    @SuppressLint("SimpleDateFormat")
    private fun onError(s: String) {
        LW.d(TAG, "Changing state to error. Cause: $s.")

        state = errorState

        val rightNow = Calendar.getInstance() // initialized with the current date and time
        val formattedDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(rightNow.time)
        lastError = "$formattedDate: msg"

        LW.d(TAG, "onError($s) called. Triggering stopAndRelease() if mp != null")
        LW.e(TAG, s)

        runOnMain {
            exoPlayer?.apply {
                LW.d(TAG, "mp is not null. stopAndRelease")
                stopAndRelease(this)
            }
        }
    }

    // endregion

    // region stop related
    fun stopWithCallback(cb: StateChangeCallback) {
        LW.d(TAG, "stop called with callback.")
        stateChangeCallback = cb
        stop()
    }

    fun stop() {
        LW.d(TAG, "stop called")

        LW.d(TAG, "Set isActive to false.")

        updateState()
        runOnMain {
            exoPlayer?.apply {
                stopAndRelease(this)
                LW.d(TAG, "Applied release preparations to media player")
            }
        }

        cleanUpService()
    }

    private fun cleanUpService() {
        lockHandler?.releaseLocks()

        LW.i(TAG, "Stopping foreground and...")
        stopForeground(true)
        LW.i(TAG, "... stopping self.")
        stopSelf()
        LW.i(TAG, "Stopping foreground and stopping self: done.")
    }

    private fun stopAndRelease(exoPlayer: ExoPlayer) {
        Handler(Looper.getMainLooper()).post {
            exoPlayer.apply {
                stop()
                release()
            }
        }

        updateState()
        this.exoPlayer = null
        LW.d(TAG, "Stopped and released exoPlayer")

        synchronized(myNoisyAudioStreamReceiverLock) {
            if (myNoisyAudioStreamReceiverIsRegistered) {
                try {
                    unregisterReceiver(myNoisyAudioStreamReceiver)
                } catch (e: IllegalArgumentException) {
                    LW.w(TAG, "myNoisyAudioStreamReceiver was not registered.")
                }
                myNoisyAudioStreamReceiverIsRegistered = false
                LW.d(TAG, "unregistered noisy audio stream receiver")
            }
        }

        synchronized(networkObserverLock) {
            if (networkObserverRegistered) {
                try {
                    unregisterReceiver(networkObserver)
                } catch (e: IllegalArgumentException) {
                    LW.w(TAG, "networkObserver was not registered.")
                }
                networkObserverRegistered = false
                LW.d(TAG, "unregistered networkObserver receiver")
            }
        }
    }
    // endregion

    // region myNoisyAudioStreamReceiver
    private lateinit var myNoisyAudioStreamReceiver: BecomingNoisyReceiver
    private var myNoisyAudioStreamReceiverIsRegistered = false
    private val myNoisyAudioStreamReceiverLock = Any()
    // endregion

    private lateinit var networkObserver: NetworkObserver
    private var networkObserverRegistered = false
    private val networkObserverLock = Any()

    fun restartPlayback(isNewNetworkFast: Boolean) {
        runOnMain {
            createExoPlayer(isNewNetworkFast).apply {
                exoPlayer?.apply {
                    release()
                }
                exoPlayer = this@apply
            }
        }
    }
    // endregion

    // region main thread wrapping
    private fun runOnMain(function: () -> Unit) {
        LW.e(TAG, "Running something on main")
        Handler(Looper.getMainLooper()).post { function() }
    }

    // endregion
    companion object {
        private const val ChannelId = "Metal Only Stream Notifications"
        private const val NotificationId = 1
        private const val NotificationChannelName = "Metal Only Stream"

        const val ACTION_PLAY = "de.ironjan.metalonly.play"
        const val ACTION_STOP = "de.ironjan.metalonly.stop"
        const val TAG = "MoStreamingService"

        private const val desktopMp3StreamUri = "https://metal-only.streampanel.cloud/stream"
        private const val mobileAacStreamUri = "https://sinful.streampanel.cloud/stream"
    }
}
