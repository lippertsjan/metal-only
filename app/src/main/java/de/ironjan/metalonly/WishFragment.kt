package de.ironjan.metalonly

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.koushikdutta.async.future.FutureCallback
import de.ironjan.metalonly.api.Client
import de.ironjan.metalonly.api.model.Stats
import de.ironjan.metalonly.api.model.Wish
import de.ironjan.metalonly.databinding.FragmentWishBinding
import de.ironjan.metalonly.log.LW

/**
 * A simple [Fragment] subclass.
 */
class WishFragment : Fragment(), StatsLoadingRunnable.StatsLoadingCallback {
    override fun onStatsLoadingError() {
        runOnUiThread { binding?.txtExplanation?.setText(R.string.message_could_not_load_show_info) }
    }

    private var binding: FragmentWishBinding? = null

    var stats: Stats? = null

    val args: WishFragmentArgs by navArgs<WishFragmentArgs>()

    override fun onStatsLoadingSuccess(stats: Stats) {
        this.stats = stats

        // TODO: extract strings to resources
        val maxNoOfWishesAsString =
            if (stats.maxNoOfWishes == 0) "unbegrenzt" else stats.maxNoOfWishes

        val maxNoOfGreetingsAsString =
            if (stats.maxNoOfGreetings == 0) "unbegrenzt" else stats.maxNoOfGreetings
        val playlistFull =
            if (stats.maxNoOfWishesReached) "Sorry, die Playlist ist bereits voll. Es sind keine weiteren Wünsche mehr möglich." else ""

        val msg =
            "Derzeitiges Limit: $maxNoOfWishesAsString Wünsche und $maxNoOfGreetingsAsString Grüße pro Hörer. Wünsche und Grüße sind nur während der moderierten Sendezeit möglich. $playlistFull"

        val show = stats.showInformation.show
        val genre = stats.showInformation.genre

        runOnUiThread {
            binding?.editArtist?.isEnabled = !stats.maxNoOfWishesReached
            binding?.editTitle?.isEnabled = !stats.maxNoOfWishesReached
            binding?.editGreeting?.isEnabled = !stats.maxNoOfGreetingsReached
            binding?.txtExplanation?.text = msg

            binding?.txtShowInfomation?.text =
                context?.getString(R.string.show_and_genre, show, genre) ?: ""
            if (show.isNullOrBlank() || genre.isNullOrBlank()) {
                binding?.txtShowInfomation?.visibility = View.INVISIBLE
            } else {
                binding?.txtShowInfomation?.visibility = View.VISIBLE
            }
        }
    }

    private fun runOnUiThread(function: () -> Unit) = activity?.runOnUiThread(function)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWishBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnClear?.setOnClickListener { clearForm() }
        binding?.btnSubmit?.setOnClickListener { submit() }

        val lContext = context ?: return
        StatsLoadingRunnable(lContext, this).run()
    }

    private fun clearForm() {
        binding?.editArtist?.text?.clear()
        binding?.editTitle?.text?.clear()
        binding?.editGreeting?.text?.clear()
    }

    private fun submit() {
        val nick = binding?.editNick?.text.toString()
        val artist = binding?.editArtist?.text.toString()
        val title = binding?.editTitle?.text.toString()
        val greeting = binding?.editGreeting?.text.toString()

        val lContext = context ?: return
        if (nick.isBlank()) {
            // TODO show validation error instead and prevent submit
            Toast.makeText(lContext, "Nick muss angegeben werden.", Toast.LENGTH_SHORT).show()
            return
        }

        if (!artist.isBlank() && title.isBlank()) {
            // TODO show validation error instead and prevent submit
            Toast.makeText(
                lContext,
                "Artist und Title müssen beide angegeben werden oder beide leer sein.",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        Client(lContext).sendWish(Wish(nick, artist, title, greeting), futureCallback)
    }

    private val futureCallback = object : FutureCallback<String> {
        override fun onCompleted(e: Exception?, result: String?) {
            if (e != null) {
                LW.e(TAG, "Exception when sending wishes.", e)
                snack("Wunsch/Gruß konnte nicht gesendet werden: " + e.message)
                return
            }
            if (result != null) {
                snack(result)
                clearForm()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(PREF_KEY_NICK, binding?.editNick?.text?.toString() ?: return)
            putString(PREF_KEY_ARTIST, binding?.editArtist?.text.toString())
            putString(PREF_KEY_TITLE, binding?.editTitle?.text.toString())
            putString(PREF_KEY_GREETING, binding?.editGreeting?.text.toString())
            commit()
        }
    }

    override fun onResume() {
        super.onResume()
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        binding?.editNick?.setText(sharedPref.getString(PREF_KEY_NICK, ""))
        binding?.editArtist?.setText(sharedPref.getString(PREF_KEY_ARTIST, ""))
        binding?.editTitle?.setText(sharedPref.getString(PREF_KEY_TITLE, ""))
        binding?.editGreeting?.setText(sharedPref.getString(PREF_KEY_GREETING, ""))

        val artistArg = args.artist
        val titleArg = args.title
        if (artistArg != null && titleArg != null) {
            binding?.editArtist?.setText(artistArg)
            binding?.editTitle?.setText(titleArg)
        }
    }

    private fun snack(s: String) {
        LW.v(TAG, "Called snack($s)")

        val context = activity ?: return
        Toast.makeText(context, s, Toast.LENGTH_LONG).show()
    }

    companion object {
        private const val TAG = "WishFragment"
        private const val PREF_KEY_NICK = "PREF_KEY_NICK"
        private const val PREF_KEY_ARTIST = "PREF_KEY_WISH"
        private const val PREF_KEY_TITLE = "PREF_KEY_TITLE"
        private const val PREF_KEY_GREETING = "PREF_KEY_GREETING"
    }
}
