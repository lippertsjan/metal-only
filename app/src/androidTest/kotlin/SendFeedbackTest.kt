package de.ironjan.metalonly;

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasCategories
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.startsWith
import org.hamcrest.Matchers.stringContainsInOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class SendFeedbackTest {

    @get:Rule
    val activityRule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun sendsFeedbackIntent() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.menu_feedback)).perform(click())

        intended(
            allOf(
                hasAction(Intent.ACTION_SENDTO),
                hasCategories(mutableSetOf(Intent.CATEGORY_DEFAULT)),
                hasExtra(Intent.EXTRA_EMAIL, developerMails),
                hasExtra(Intent.EXTRA_SUBJECT, startsWith(SUBJECT_TEMPLATE)),
                hasExtra(
                    Intent.EXTRA_TEXT,
                    stringContainsInOrder(setOf(FEEDBACK, ADDITIONAL_INFO, ANDROID_VERSION))
                ),
            )
        )
    }
}