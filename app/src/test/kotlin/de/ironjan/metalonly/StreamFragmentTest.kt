package de.ironjan.metalonly

import de.ironjan.metalonly.streaming.ExoPlayerInternalState
import de.ironjan.metalonly.streaming.ExoPlayerState
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class StreamFragmentTest {
    /*
        val isBufferCircleRunning = getIsBufferCicleRunning(state);
        val qualityString = getQualityString
     */
    @ParameterizedTest
    @MethodSource("getExoPlayerStatesForPlayButton")
    fun should_be_play_icon(internalState: ExoPlayerInternalState) {
        val state = ExoPlayerState(internalState, false, false, false)

        val actual = StreamFragment.getNewPlayButtonDrawable(state, R.drawable.ic_action_play)

        assertEquals(R.drawable.ic_action_play, actual)
    }

    @Test
    fun should_be_stop_icon() {
        val state = ExoPlayerState(ExoPlayerInternalState.Ready, false, false, false)

        val actual = StreamFragment.getNewPlayButtonDrawable(state, R.drawable.ic_action_play)

        assertEquals(R.drawable.ic_action_stop, actual)
    }

    @Test
    fun should_be_buffering_icon() {
        val state = ExoPlayerState(ExoPlayerInternalState.Buffering, false, false, false)

        val actual = StreamFragment.getNewPlayButtonDrawable(state, R.drawable.ic_action_play)

        assertEquals(R.drawable.ic_stream_loading, actual)

    }

    @Test
    fun should_keep_last_icon() {
        val state = ExoPlayerState(ExoPlayerInternalState.Unknown, false, false, false)

        // Use a drawable that's not stream related to ensure the function is working correctly
        val actual = StreamFragment.getNewPlayButtonDrawable(state, R.drawable.ic_action_settings)

        assertEquals(R.drawable.ic_action_settings, actual)

    }

    @Test
    fun should_be_buffering() {
        val state = ExoPlayerState(ExoPlayerInternalState.Buffering, false, false, false)

        val actual = StreamFragment.getIsBufferCircleRunning(state)

        assertTrue(actual)
    }

    @Test
    fun should_not_be_buffering() {
        val state = ExoPlayerState(ExoPlayerInternalState.Error, false, false, false)

        val actual = StreamFragment.getIsBufferCircleRunning(state)

        assertFalse(actual)
    }

    @Test
    fun should_be_high_quality() {
        val state = ExoPlayerState(ExoPlayerInternalState.Error, false, false, true)

        val actual = StreamFragment.getQualityText(state)

        assertEquals(R.string.high_quality, actual)
    }

    @Test
    fun should_not_be_high_quality() {
        val state = ExoPlayerState(ExoPlayerInternalState.Error, false, false, false)

        val actual = StreamFragment.getQualityText(state)

        assertEquals(R.string.low_quality, actual)
    }

    @Test
    fun should_be_mobile_stream() {
        val state = ExoPlayerState(ExoPlayerInternalState.Error, false, false, false)

        val actual = StreamFragment.getQualityHint(state)

        assertEquals(R.string.mobile_stream, actual)
    }

    @Test
    fun should_be_desktop_stream() {
        val state = ExoPlayerState(ExoPlayerInternalState.Error, false, false, true)

        val actual = StreamFragment.getQualityHint(state)

        assertEquals(R.string.desktop_stream, actual)
    }

    companion object {
        @JvmStatic
        fun getExoPlayerStatesForPlayButton(): Stream<ExoPlayerInternalState> {
            return Stream.of(
                ExoPlayerInternalState.Ended,
                ExoPlayerInternalState.Idle,
                ExoPlayerInternalState.Error
            )
        }

    }
}