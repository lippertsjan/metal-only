package de.ironjan.metalonly.streaming

import androidx.media3.common.Player
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PlayerStateExtractorTest {
    var player: Player = mock()

    @Test
    fun should_map_idle_state() {
        whenever(player.playbackState).doReturn(Player.STATE_IDLE);

        val actual = PlayerStateExtractor.getStateFrom(player, false).state

        assertEquals(ExoPlayerInternalState.Idle, actual)
    }

    @Test
    fun should_map_buffering_state() {
        whenever(player.playbackState).doReturn(Player.STATE_BUFFERING);

        val actual = PlayerStateExtractor.getStateFrom(player, false).state

        assertEquals(ExoPlayerInternalState.Buffering, actual)
    }

    @Test
    fun should_map_ready_state() {
        whenever(player.playbackState).doReturn(Player.STATE_READY);

        val actual = PlayerStateExtractor.getStateFrom(player, false).state

        assertEquals(ExoPlayerInternalState.Ready, actual)
    }

    @Test
    fun should_map_ended_state() {
        whenever(player.playbackState).doReturn(Player.STATE_ENDED);

        val actual = PlayerStateExtractor.getStateFrom(player, false).state

        assertEquals(ExoPlayerInternalState.Ended, actual)
    }

    @Test
    fun should_default_to_unknown_state() {
        whenever(player.playbackState).doReturn(25);

        val actual = PlayerStateExtractor.getStateFrom(player, false).state

        assertEquals(ExoPlayerInternalState.Unknown, actual)
    }

    @Test
    fun should_map_isPlaying_as_true() {
        whenever(player.isPlaying).doReturn(true);

        val state = PlayerStateExtractor.getStateFrom(player, false)

        assertTrue(state.isPlaying)
    }

    @Test
    fun should_map_isPlaying_as_false() {
        whenever(player.isPlaying).doReturn(false);

        val state = PlayerStateExtractor.getStateFrom(player, false)

        assertFalse(state.isPlaying)
    }

    @Test
    fun should_map_isLoading_as_true() {
        whenever(player.isLoading).doReturn(true)

        val state = PlayerStateExtractor.getStateFrom(player, false)

        assertTrue(state.isLoading)
    }

    @Test
    fun should_map_isLoading_as_false() {
        whenever(player.isLoading).doReturn(false)

        val state = PlayerStateExtractor.getStateFrom(player, false)

        assertFalse(state.isLoading)
    }

    @Test
    fun should_map_isHighQuality_as_true() {
        val state = PlayerStateExtractor.getStateFrom(player, true)

        assertTrue(state.isHighQuality)
    }

    @Test
    fun should_map_isHighQuality_as_false() {
        val state = PlayerStateExtractor.getStateFrom(player, false)

        assertFalse(state.isHighQuality)
    }
}