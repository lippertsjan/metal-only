#!/bin/bash
set -e

# lintDebug:
./gradlew -Pci --console=plain :app:lintDebug

# assembleDebug:
./gradlew assembleDebug

# debugTests:
./gradlew -Pci --console=plain :app:testDebug