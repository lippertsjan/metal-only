#[macro_use]
extern crate error_chain;

use tokio::io::{AsyncWriteExt};
use std::path::Path;

extern crate futures;
extern crate tokio;

error_chain! {
     foreign_links {
         Io(std::io::Error);
         HttpRequest(reqwest::Error);
     }
}


fn main() {
    let rt = tokio::runtime::Runtime::new().unwrap();

    match rt.block_on(app()) {
        Ok(_) => println!("Done"),
        Err(e) => println!("An error ocurred: {}", e),
    };
}

async fn app() -> Result<()> {
    let url = "https://www.metal-only.de/team.html";
    let text = download_to_text(url).await?;

    let mods = extract_mods(text.as_str());

    let x =
        mods.into_iter()
            .map(move |m|  {
                let fut = tokio::spawn(
                    dl_mod_img(m)
                );

                fut
            }).collect::<Vec<_>>();
    futures::future::join_all(x).await;

    Ok(())
}

async fn download_to_text(url: &str) -> Result<String> {
    let response = reqwest::get(url).await?;
    let text = response.text().await?;
    Ok(text)
}

#[derive(Debug)]
struct Mod {
    name: String,
    url: String,
}

async fn dl_mod_img(m: Mod) -> Result<()> {
    let base = "https://www.metal-only.de";

    let abs_url = format!("{}{}", &base, &m.url);
    let response = reqwest::get(&abs_url).await?;
    let bytes = response.bytes().await?;

    let file_name = format!("{}.jpg", &m.name).to_lowercase();
    let relative_path_in_project = "../app/src/main/res/drawable/";
    let path = Path::new(relative_path_in_project).join(file_name);

    let mut file = tokio::fs::File::create(path).await?;
    file.write_all(bytes.as_ref()).await?;


    Ok(())
}

fn extract_mods(text: &str) -> Vec<Mod> {
    use scraper::Html;
    use scraper::Selector;

    let html = Html::parse_document(text);
    let fragment = html;
    let ul_selector = Selector::parse("ul.list-team").unwrap();
    let li_selector = Selector::parse("li").unwrap();

    let div_selector = Selector::parse("div.ti").unwrap();
    let img_selector = Selector::parse("img").unwrap();

    let ul = fragment.select(&ul_selector).next().unwrap();
    let som = ul.select(&li_selector).map(|li| {
        let div = li.select(&div_selector).next().unwrap();
        let div_text = div.text().next();//.collect::<Vec<_>>();

        let img = li.select(&img_selector).next().unwrap();
        let img_url = img.value().attr("src");

        match (div_text, img_url) {
            (Some(name), Some(url)) => Some(Mod { name: name.to_string(), url: url.to_string() }),
            _ => None
        }
    })
        .filter(|o| o.is_some())
        .map(|o| o.unwrap())
        .collect::<Vec<_>>();

    som
}