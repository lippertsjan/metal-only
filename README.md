# Metal Only Android App

* LICENSE: [Apache-Lizenz, Version 2.0.](https://gitlab.com/lippertsjan/metal-only/-/blob/main/LICENSE.txt)

* AUTHORS: `git log --all --format='%aN <%cE>' | sort -u`
* IMAGE RIGHTS: All images rights for images under images_sources belong to Metal Only e.V. and are  used with permission.
  See https://www.metal-only.de/impressum.html for more information.

Diese App bringt den Internet Radio Stream von http://metal-only.de/ auf Android, wo man 24 Stunden am Tag Rock und Metal hören kann. Außerdem enthält
sie einige Sender-spezifische Funktionen, wie das Senden von Musik-Wünschen oder Grüßen.

Die App kann über Google Play [heruntergeladen](https://play.google.com/store/apps/details?id=com.codingspezis.android.metalonly.player) werden. 
Probleme und Feature-Wünsche können an [lippertsjan+metal-only@gmail.com](mailto:lippertsjan+metal-only@gmail.com) gesendet werden.

Ursprünglich wurde die App von [@rbrtslmn](https://github.com/rbrtslmn) und [@michael-busch](https://github.com/michael-busch) entwickelt. News gibt es primär unter 
[http://ironjan.de/metal-only](http://ironjan.de/metal-only).

## Table of Contents

* [Helping](#helping)
    * [Send Feedback and Report Errors](#send-feedback-and-report-errors)
    * [Help with App Development](#help-with-app-development)
        * [Getting Started](#getting-started)

## Helping

### Send Feedback and Report Errors

The easiest option is to use the feedback functionality in the app. It automatically includes the most important information in the email template. If
it's not possible to use the feedback function, try to answer the following questions:

* Which device do you have and which Android version is running on it?
* What did you do?
* What happened and what would you exptected to happen instead?

You can add the issues directly to the issue tracker: [https://gitlab.com/lippertsjan/metal-only/-/issues](https://gitlab.com/lippertsjan/metal-only/-/issues)

### Help with App Development

We're using the [gradle](http://tools.android.com/tech-docs/new-build-system/user-guide) build system. I strongly recommend [Android Studio](https://developer.android.com/sdk/index.html) to develop.

### Getting started

1. Fork this project.
2. Import the project into android studio (Check out from Version Control, select github or git)
3. Create a branch to work on. Then fix bugs, implement features, ...
4. Push on your fork
5. Create a Pull request with base branch `main`

See also https://gun.io/blog/how-to-github-fork-branch-and-pull-request/ -- the blog post refers to Github but the process is very similar on Gitlab.

#### Signing etc.

You can create the following environment variables:

```
metalonly_signing=/some/path/to/a-file-collection
metalonly_publishkey=/home/ljan/.signing/metalonly_serviceaccount.json
```

`file-collection` refers to similarly named files residing in the same folder, e.g. `metalonly_signing.gradle` and `metalonly_signing.keystore`. The 
property `metalonly_signing` can be re-used to point to the key-store etc. Please refer to the build.gradle files which additionals exist. Here are 
some templates for the ones currently used:

```
// metalonly.signing
android {
  signingConfigs {
    release {
      storeFile file(System.getenv("metalonly_signing")+".keystore")
      storePassword "replace-me"
      keyAlias "replace-me"
      keyPassword "replace-me"
    }
  }

  buildTypes {
    release {
      signingConfig signingConfigs.release
    }
  }
}
```

# ktlint

```
gradle ktlintCheck
gradle ktlintFormat
```

# Publishing

* Adapt app/build.gradle
* Use publish_release.sh (relies on https://github.com/Triple-T/gradle-play-publisher)
    * Currently broken. Use `gradle connectedAndroidTest`
    * Currently broken. Use `gradle publishReleaseBundle`
* Promoting releases: `gradle promoteArtifact`
    * Possible:  `--release-status inProgress --user-fraction $FRACTION`


# Useful commands

```
./gradlew dependencyUpdates
```
